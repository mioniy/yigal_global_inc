/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#ifdef __IAR__
#define  CONST                         flash
#else
#define  CONST                         const
#define  strcpy_P                      strcpy
#define  strlen_P                      strlen
#define  strcmp_P                      strcmp
#endif

#define  CHAR                          0
#define  USINT                         1
#define  ULONG                         2
#define  STRING                        3

#define  TRUE                          (bool)1
#define  FALSE                         (bool)0

#define  YES                           (bool)1
#define  NO                            (bool)0

#define  ON                            (bool)1
#define  OFF                           (bool)0

#define  ENABLED                       (bool)1
#define  DISABLED                      (bool)0

#define  MASTER                        (bool)1
#define  SLAVE                         (bool)0

#define  CLOSED                        (bool)1
#define  OPENED                        (bool)0

#define  PRESSED                       (bool)1
#define  RELEASED                      (bool)0

#define  WRITE                         (bool)1
#define  READ                          (bool)0

#define  ACCEPT                        (bool)1
#define  RETURN                        (bool)0

#define  ACTIVE                        (bool)1
#define  INACTIVE                      (bool)0

#define  ACTIVATE                      (bool)1
#define  DEACTIVATE                    (bool)0

#define  START                         (bool)1
#define  STOP                          (bool)0

#define  BIT_0                         0x01
#define  BIT_1                         0x02
#define  BIT_2                         0x04
#define  BIT_3                         0x08
#define  BIT_4                         0x10
#define  BIT_5                         0x20
#define  BIT_6                         0x40
#define  BIT_7                         0x80
#define  BIT_8                         0x0100
#define  BIT_9                         0x0200
#define  BIT_10                        0x0400
#define  BIT_11                        0x0800
#define  BIT_12                        0x1000
#define  BIT_13                        0x2000
#define  BIT_14                        0x4000
#define  BIT_15                        0x8000
#define  BIT_16                        0x00010000
#define  BIT_17                        0x00020000
#define  BIT_18                        0x00040000
#define  BIT_19                        0x00080000
#define  BIT_20                        0x00100000
#define  BIT_21                        0x00200000
#define  BIT_22                        0x00400000
#define  BIT_23                        0x00800000
#define  BIT_24                        0x01000000
#define  BIT_25                        0x02000000
#define  BIT_26                        0x04000000
#define  BIT_27                        0x08000000
#define  BIT_28                        0x10000000
#define  BIT_29                        0x20000000
#define  BIT_30                        0x40000000
#define  BIT_31                        0x80000000

#define  SET_BIT(Var,Val)              (Var) |= (Val)
#define  CLEAR_BIT(Var,Val)            (Var) &= ~(Val)
#define  BIT_IS_SET(Var,Val)           (((Var) & (Val)) == (Val))
#define  BIT_IS_CLEAR(Var,Val)         (((Var) & (Val)) != (Val))
#define  ANY_BIT_IS_SET(Var,Val)       (((Var) & (Val)) > 0)

#define  SOH                           0x01
#define  STX                           0x02
#define  ETX                           0x03
#define  EOT                           0x04
#define  ENQ                           0x05
#define  ACK                           0x06
#define  BEL                           0x07
#define  BS                            0x08
#define  TAB                           0x09
#define  LF                            0x0A  // '\n'
#define  VT                            0x0B
#define  FF                            0x0C
#define  CR                            0x0D  // '\r'
#define  SO                            0x0E
#define  SI                            0x0F
#define  DLE                           0x10
#define  DC1                           0x11
#define  DC2                           0x12
#define  DC3                           0x13
#define  DC4                           0x14
#define  NAK                           0x15
#define  SYN									0x16
#define  ETB									0x17
#define  CAN									0x18
#define  EM									   0x19
#define  USB									0x1A
#define  ESC									0x1B
#define  FS   									0x1C
#define  GS   									0x1D
#define  RS   									0x1E
#define  US   									0x1F

#define  CTRL_Z                        USB   // 0x1A
#define  SPACE									' '   // 0x20
#define  SLASH                         '/'   // 0x2F
#define  COLON									':'   // 0x3A
#define  SEMI_COLON							';'   // 0x3B
/*---------------------------- Typedef Directives ----------------------------*/
typedef  unsigned char                 byte;
typedef  unsigned char                 bool;
typedef  unsigned int                  usint;
typedef  unsigned int                  uint;
typedef  unsigned long                 ulong;

                  /***********************/
                  /* Task Timer settings */
                  /***********************/
#define  TIME_INFINITE                 (unsigned)(-1)

typedef	usint						         tTimerMsec;
typedef	ulong						         tTimerSec;
typedef	ulong						         tTimerMin;
typedef	ulong						         tTimerHour;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
/*----------------------------------------------------------------------------*/
/*-------------------------- External Variables ------------------------------*/
/*----------------------------------------------------------------------------*/

